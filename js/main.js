$(function(){


    $('#boxes .card').hover(function(){
        $(this).find('h4').addClass('border-primary');
    }, 
    function(){
        $(this).find('h4').removeClass('border-primary');
    });


    $('.image-container').hover(function(){
        $(this).find('.teaser-overlay').css('opacity', 0.1);
        $(this).find('.top-left').addClass('btn-overlay');
    }, function(){
        $(this).find('.teaser-overlay').css('opacity', 0.7);
        $(this).find('.top-left').removeClass('btn-overlay');
    });


    

});